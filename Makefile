VERSION := $(shell date +"%y.%m.%d")

.DEFAULT_GOAL := build
.PHONY: clean fmt test vet build rpmdev archive rpm

clean:
	@go clean

fmt:
	@go fmt ./...

test:
	@go test -short ./...

vet: fmt
	@go vet ./...

build: vet
	@CGO_ENABLED=0 go build -ldflags "-X 'main.appVersion=$(VERSION)'"

# The RPM targets are no longer required for my purposes and I don't have any RHEL machines to
# test if they are still working but I'm leaving them in case they are of use to others. 

rpmdev:
	rpmdev-setuptree

archive:
	mkdir -p ~/rpmbuild/SOURCES
	git archive --format=tar.gz --prefix=lwr-$(VERSION)/ -o ~/rpmbuild/SOURCES/lwr-$(VERSION).tar.gz HEAD

rpm: rpmdev archive
	rpmbuild -ba lwr.spec
	ls -1 ~/rpmbuild/RPMS/x86_64/
